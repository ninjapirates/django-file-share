from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from .forms import (RegistrationForm, LoginForm)

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    request.session.set_expiry(86400)
                    login(request,user)
                    return HttpResponseRedirect(reverse('main:home'))
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
        # TODO: have these^ return the login page with a modified context indicating the error!
    else:
        form = LoginForm()
        args = {'form': form}
    return render(request, 'sfs_auth/login.html',args)

#-------------------------------------------------
#The user_logout function will handle the log out
# functionality and render the logged out message
#------------------------------------------------
def user_logout(request):
    logout(request)
    return redirect('main:home')

def register(request):
    if request.method =='POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('auth:login'))
    else:
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'sfs_auth/register.html', args)
