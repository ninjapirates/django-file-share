from django.test import TestCase, Client
from django.urls import reverse
from django.http import HttpResponseRedirect

from django.contrib.auth.models import User

# IMPORTANT DOCUMENTATION:
#
# - Tutorial section on testing:
#   https://docs.djangoproject.com/en/2.0/intro/tutorial05/
#
# - How to write and run tests (using TestCase, setUp, 'manage.py test', etc.)
#   https://docs.djangoproject.com/en/2.0/topics/testing/overview/
#
# - Testing tools (using Client, Response, alternative TestCase subclasses, etc.)
#   https://docs.djangoproject.com/en/2.0/topics/testing/tools/

# As our tests expand, we'll need to break our tests out into multiple files and classes.

# 1) Extend the TestCase class -
# this is like using the "TestFixture" attribute in NUnit
class RegistrationTests(TestCase):

	# 2) subclasses of TestCase can override setUp and tearDown methods
	# that run once before or after all tests
	def setUp(self):
		# this is just some POST data I'll re-use several times
		self.fake_user_data = {
			'first_name': 'Tim',
            'last_name': 'Tester',
            'username': 't.tester',
            'email': 'timtest@fake.com',
            'password1': 'notarealpassword',
            'password2': 'notarealpassword'
		}

	# 3) class methods that begin with "test" are run as tests,
	# like using the "Test" attribute on a method in NUnit

	def test_register_uses_specific_template(self):
		# 4) "Client" is a class that you can use to make fake requests
		# (these requests skip actual HTTP and interact directly with Django)
		c = Client()
		url = reverse('register')
		response = c.get(url)

		# In this case, we can check the response to verify the template
		# that was used to render the registration page.
		# THIS IS NOT A GOOD TEST, because it is testing implementation instead of behaviour!
		templates = [t.name for t in response.templates]

		self.assertIn('sfs_auth/register.html', templates)

		# if you want to verify the context data a template was rendered with,
		# you can access it with `response.context`


	def test_register_redirects_to_login(self):

		# post that data the register, and tell the client to follow redirects
		c = Client()
		url = reverse('register')
		response = c.post(url, follow=True, data=self.fake_user_data)

		# we should be redirected to the login page:
		expected = [(reverse('index'), 302)]
		# with follow=True, redirect_chain is set, otherwise it is not
		self.assertEqual(response.redirect_chain, expected)

	def test_register_creates_a_user(self):
		c = Client()

		# modify the user data a little so we can rule out the previous test interfering
		# (now that I've checked - it doesn't.)
		our_fake_user_data = self.fake_user_data
		our_fake_user_data['username'] = 't.tester2'

		response = c.post(reverse('register'), data=our_fake_user_data)

		# not necessary, just trying out type comparisons
		self.assertIs(type(response), HttpResponseRedirect)

		# you can print in a test, and it will appear in test output
		# after the tests start and before the test results
		print(User.objects.all())

		# currently using username as primary key, so we can try to get
		# the user we just registered like this:
		newUser = User.objects.get(username='t.tester2')
		self.assertIsNotNone(newUser, msg='Wanted a specific user but didn\'t get one...')


	def test_registered_user_can_authenticate(self):
		c = Client()
		# I don't care about the response this time
		c.post(reverse('register'), self.fake_user_data)

		# this works because we're using Django's built-in authentication backend:
		# they provide testing helpers to emulate logging in. If we were not using
		# the framework's auth system, this would do zilch

		didLogin = c.login(username='t.tester', password='notarealpassword')
		self.assertTrue(didLogin)

		# so this checks that Django will successfully authenticate the user we just
		# created using our registration view. It is *not* directly checking a "login/"
		# endpoint of our own!

		# once we use c.login() or c.force_login(), we can call c.get/post/whatever
		# on pages that are only accessible to logged in users, and the test would
		# be able to access those.






