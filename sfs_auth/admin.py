from django.contrib import admin
from .models import FileShareUsers

# Register your models here.
#We Will have to change these fields to reflect the fields of files being uploaded to the
#file share site
class FileShareAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'username','email','password']
admin.site.register(FileShareUsers, FileShareAdmin)