from django.db import models
from .forms import (RegistrationForm)

# Create your models here.
class FileShareUsers(models.Model):

    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    username = models.CharField(max_length=50, primary_key=True)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=500)

    def __str__(self):
        return self.username


