from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'sfs_auth'
