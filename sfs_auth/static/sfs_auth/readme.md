# The static folder #

This folder is for any static assets that are *particular to the sfs_auth module*. Static assets (CSS, JS, images, fonts, etc.) that are used by several different modules should go in the `<project-root>/static/` folder.

## Why the weird path? ##

When different modules each have their own static files directory, say

+ `project/sfs_main/static/`
+ `project/sfs_auth/static/`

Django combines the files in these directories into one when we deploy, so all static assets can be served from the same location. This means that if both of those directories each contained a file named "styles.css", and you try to load that in a template with `{% static "styles.css" %}`, DJango *won't know which file you're referring to*.

The solution is to nest module-specific assets inside an additional directory that is the same name as the module name. Just like with templates, this provides namespacing. The asset will now be in `/project/<module>/static/<module>/`, and in a template you can load it with `{% static "<module>/filename" %}` and Django will know which file you mean even if two different modules have a static file with the same filename.