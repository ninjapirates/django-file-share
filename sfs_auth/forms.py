from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


#class UploadFileForm(forms.Form):
#    title = forms.CharField(max_length=100)
#    file = forms.FielField()
class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

#class ViewandChooseFiles()
#   conn = sqlite.connect('database file name')
#   cur = conn.cursor()
#   cur.execute('SELECT * FROM files')
#   for row in cursor:
#       print 'File:', row[0]
#        print 'FileName:', row[1]
#

class RegistrationForm(UserCreationForm):
    username = forms.CharField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
            'password1',
            'password2'
        )
    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.username = self.cleaned_data['username']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()
        return user


