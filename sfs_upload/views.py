from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.shortcuts import redirect, render
from django.urls import reverse_lazy

from .forms import DocumentForm
from .models import Document

# View uploaded files
@login_required(login_url=reverse_lazy('auth:login'))
def file(request):
	documents = Document.objects.all()
	return render(request, 'sfs_upload/files.html', {'documents': documents})

@login_required(login_url=reverse_lazy('auth:login'))
def upload(request):
	if request.method == 'POST':
		form = DocumentForm(request.POST, request.FILES)
		if form.is_valid():
			# as a shortcut, could call form.save() here and be done,
			# but we'll create the model instance manually so that
			# we can inject the user who uploaded the document
			document = Document(
				user = request.user,
				description = request.POST['description'],
				document = request.FILES['document']
				)
			document.save()
			# if the document was saved, go to the "main" upload page so
			# we can see what was uploaded
			return redirect('main:home')
	else:
		form = DocumentForm()
	return render(request, 'sfs_upload/model_form_upload.html', {'form': form})
