from django.urls import path

from . import views

app_name='upload'
urlpatterns = [
	path('', views.file, name='file'),
	path('upload', views.upload, name='upload'),
]
