from django import forms
from sfs_upload.models import Document

class DocumentForm(forms.ModelForm):
	class Meta:
		model = Document
		fields = ('description', 'document',)
