from django.apps import AppConfig

class SfsUploadConfig(AppConfig):
    name = 'sfs_upload'
