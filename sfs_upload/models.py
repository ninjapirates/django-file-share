from django.db import models
from django.contrib.auth.models import User, AnonymousUser

def generateFilepath(instance, filename):
	# will store the upload in MEDIA_ROOT/uploads/<userid>/<id>-<filename>
	return 'uploads/{0}/{1}-{2}'.format(instance.user.id, instance.id, filename)

# Create your models here.
class Document(models.Model):
	# the date and time the file was uploaded
	uploaded_at = models.DateTimeField(auto_now_add=True)
	# the user who uploaded the file - I'd rather not allow nulls, but it was
	# necessary for the automatic migration to work
	user = models.ForeignKey(User, models.CASCADE, null=True)
	# a user-provided description of the file (defaults to blank)
	description = models.TextField(max_length=600, blank=True)
	# the file that was uploaded
	document = models.FileField(upload_to='documents/')

