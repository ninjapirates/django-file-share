from django.contrib import admin
from .models import Document

# Register your models here.
# We Will have to change these fields to reflect the fields of
# files being uploaded to the file share site
class DocumentAdmin(admin.ModelAdmin):
	list_display = ['description', 'document', 'uploaded_at', 'user']

admin.site.register(Document, DocumentAdmin)
