from django.apps import AppConfig


class SfsMainConfig(AppConfig):
    name = 'sfs_main'
