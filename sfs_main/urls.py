from django.urls import path

from . import views

# Note: this app_name does NOT exactly match the actual name of the module!
# (main vs. sfs_main)
# It may be better to have them match for consistency... I'm not sure what
# the best practice is. For an initial attempt, I think it might be nice to
# be able to type "main/home" instead of "sfs_main/home" all over the place.
app_name='main'
urlpatterns = [
	path('', views.home, name='home'),
]

