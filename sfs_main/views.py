from django.shortcuts import render
from django.contrib.auth import get_user
from sfs_upload.models import Document

# global home page
def home(request):
	user=get_user(request)
	if user.is_authenticated:
		files = Document.objects.order_by('-uploaded_at').filter(user__id__exact=user.id)
		return render(request, 'sfs_main/dashboard.html', {'files': files})
	else:
		return render(request, 'sfs_main/welcome.html')

